"use strict";

let pestoIO = {};
pestoIO.FileSystem = null;

var errorHandler = (err) => { console.log("ERROR: " + JSON.stringify(err)); }; 

pestoIO.InitFile = function(path)
{

}

pestoIO.ExistFile = function(path, callback)
{
    pestoIO.FileSystem.root.getFile(path, {}, function (fileEntry) 
    {
        callback(true, path);
    }, function (err) 
    { 
        callback(false, path);
        if(err.code != 1) console.log(err);
    });
}

pestoIO.ReadFile = function(path, callback)
{
    pestoIO.FileSystem.root.getFile(path, {}, function (fileEntry) 
    {
        fileEntry.file(function (file) 
        {
            var reader = new FileReader();

            reader.onloadend = function (e) { callback(e); };

            reader.readAsText(file);
        }, errorHandler);
    }, errorHandler);
}

pestoIO.WriteFile = function(path, data)
{
    pestoIO.FileSystem.root.getFile(path, { create: true }, function (fileEntry) 
    {
        fileEntry.createWriter(function (fileWriter) 
        {
            console.log("here");
            fileWriter.onwriteend = function (e) 
            {
                console.log('Write completed.');
            };

            fileWriter.onerror = function (e) 
            {
                console.log('Write failed: ' + e.toString());
            };

            var blob = new Blob([data], { type: 'text/plain' });

            fileWriter.write(blob);
        }, errorHandler);
    }, errorHandler);
}

pestoIO.RemoveWriteFile = function(path, data)
{
    pestoIO.RemoveFile(path, function()
    {
        pestoIO.WriteFile(path, data);
    });
}

/*
pestoIO.WriteFile = function(path, data)
{
    pestoIO.RemoveFile(path, function()
    {
        pestoIO.FileSystem.root.getFile(path, { create: true }, function (fileEntry) 
        {
            fileEntry.createWriter(function (fileWriter) 
            {
                console.log("here");
                fileWriter.onwriteend = function (e) 
                {
                    console.log('Write completed.');
                };

                fileWriter.onerror = function (e) 
                {
                    console.log('Write failed: ' + e.toString());
                };

                var blob = new Blob([data], { type: 'text/plain' });

                fileWriter.write(blob);
            }, errorHandler);
        }, errorHandler);
    });
}
*/

pestoIO.RemoveFile = function(path, callback)
{
    pestoIO.FileSystem.root.getFile(path, {create: false}, function(fileEntry) 
    {
        fileEntry.remove(function() 
        {
            callback();
        }, errorHandler);
    }, errorHandler);
}

/*
    { permision }
*/
// TODO: 
pestoIO.Setup = function(a, b, c, d, settings)
{
    //window.requestFileSystem(window.PERSISTENT, 1024*1024, onInitFs, errorHandler);
    //window.requestFileSystem(window.TEMPORARY, 1024*1024, onInitFs, errorHandler);
    console.log("setup");
    window.requestFileSystem(a, b, c, d);
}

// default setup
window.requestFileSystem  = window.requestFileSystem || window.webkitRequestFileSystem;
window.addEventListener('filePluginIsReady', function () 
{ 
    //pestoIO.Setup(window.PERSISTENT, 1024*1024, (fs)=> { pestoIO.FileSystem = fs; }, errorHandler); 
}, false);