"use strict";

/* laden falls vorhanden */
let pestoStorage = new Storage("pestoStorage", {});

/*
    <Example>
    pestoStorage.setItem("player", { name:"pidi", health:4 } );
    pestoStorage.getItem("player");                                 // -> { name:"pidi", health:4 }
    pestoStorage.getByPath("player/name")                           // -> "pidi"
    //pestoStorage.GetByPathSafe("player/health", 0);               // if not existing then return 0
    </Example>
*/
function Storage(name, value)
{
    if(typeof(value) != "object") throw "Error " + typeof(value);

    // Attributes
    this.Type = "PestoStorage";     // string
    this.Name = name;           // string
    this.Value = value;         // object

    
}

// -> null : bad | object / any : good
Storage.prototype.getItem = function(key)
{
    return (this.Value[key] != undefined) ? this.Value[key] : null;
}

Storage.prototype.setItem = function(key, value)
{
    this.Value[key] = value;
}

// ["array", "0"]
// -> null : bad | object / any : good
Storage.prototype.getByPath = function(path)
{
    if(typeof(path) == "string") path = path.split(/[\s./]/);
    let value = this.Value;
    
    for (let i = 0; i < path.length; i++) 
    {
        if(value == undefined) return null;
        value = value[path[i]];
    }
    
    return (value == undefined) ? null : value;
}

Storage.prototype.getByPath = function(path, set)
{
    if(typeof(path) == "string") path = path.split(/[\s./]/);
    let parent;
    let value = this.Value;
    
    for (let i = 0; i < path.length; i++) 
    {
        parent = value;
        value = value[path[i]];
    }
    
    if(value != undefined)
    {
        parent[path[path.length-1]] = set;
    }
    else
    {
        throw "path is incorrect";
    }
}

Storage.prototype.getByPathSafe = function(path, alternative)
{
    let output = this.GetByPath(path);
    return (output != null) ? output : alternative;
}