/* PhoneGap */


var $discoverButton;
document.addEventListener("deviceready", onDeviceReady, false);
function onLoad() {
    /* DOM Loaded */
    $discoverButton = document.querySelector("#discover-button")
}
function onDeviceReady() {

    //load();
    if (screen.lockOrientation != undefined)
        screen.lockOrientation('portrait');
    if (window.screen.orientation.lock != undefined)
        window.screen.orientation.lock('portrait').catch(() => { console.log("Portrait Mode not available here.") })

    pestoIO.Setup(window.PERSISTENT, 1024 * 1024, (fs) => { pestoIO.FileSystem = fs; initStorage(); }, errorHandler);

}
/* Database */
var items = [
    { id: 1, name: "Äste", color: "#FFFFFF" },
    { id: 2, name: "Holzstämme" },
    { id: 3, name: "Steine" }
];
var game = {
    player: {
        version: 100072,
        itemaccesslevel: 1, money: 0, inventory: [
            { amount: 10, parentitem: items[0], posgain: 3, activgain: 0, posgainLevel: 0, activgainLevel: 0, posgainBasePrice: 2, activgainBasePrice: 24 },
            { amount: 10, parentitem: items[1], posgain: 0, activgain: 0, posgainLevel: 0, activgainLevel: 0, posgainBasePrice: 3, activgainBasePrice: 48 },
            { amount: 10, parentitem: items[2], posgain: 0, activgain: 0, posgainLevel: 0, activgainLevel: 0, posgainBasePrice: 6, activgainBasePrice: 128 }
        ],
        temporaryInventory: [
            { amount: 0, amountonce: 0, parentitem: items[0] },
            { amount: 0, amountonce: 0, parentitem: items[1] },
            { amount: 0, amountonce: 0, parentitem: items[2] }
        ],
        temporaryInventoryActive: false
    },

}
function PriceCalc(level, baseprice) {
    return level * baseprice + baseprice;
}

function createFile(dirEntry, fileName, isAppend) {
    // Creates a new file or returns the file if it already exists.
    dirEntry.getFile(fileName, { create: true, exclusive: false }, function (fileEntry) {
        writeFile(fileEntry, null, isAppend);
    }, onErrorCreateFile);
}

function initStorage() {
    console.log("init");
    pestoIO.ReadFile("save.txt", function (e) {
        //console.log(e.srcElement.result);
        //console.log("inside");

        console.log(e.target);
        console.log(JSON.stringify(e));

        if (e.target.result != "") {
            pestoStorage = new Storage("pestoStorage", JSON.parse(e.target.result));
        }
    });
}

function save() {
    //NativeStorage.setItem("player", player);

    //console.log("player:" + player);
    pestoStorage.setItem("player", JSON.stringify(game.player));
    //localStorage.setItem("player", JSON.stringify(player));

    //pestoIO.WriteFile("save.txt", JSON.stringify(pestoStorage.Value));

    let data = JSON.stringify(pestoStorage.Value);

    // data
    pestoIO.ExistFile("save.txt", function (success, path) {
        if (success) {
            pestoIO.RemoveWriteFile(path, data);
        }
        else {
            pestoIO.WriteFile(path, data);
        }
    });
}

function load() {

    //console.log(pestoStorage.getItem("player"));
    let result;

    result = JSON.parse(pestoStorage.getItem("player"));

    if (result != undefined) {
        game.player = result;
        Vue.set(app, "player", game.player);
        Vue.set(app, "contents", game.player.inventory);
    }
}


//Active GameLoop
var dateBefore = Date.now();
setInterval(function () {
    game.player.inventory.forEach(element => {
        element.amount += element.activgain * (dateBefore - Date.now()) / 1000 * -1;
    });
    dateBefore = Date.now();
    //save(player);
}, 50 / 1000);

//Button functions

function sell(item) {
    game.player.money += Math.floor(item.amount) * 2;
    item.amount = 0;
}
function upgradeAccessLevel() {
    console.log(PriceCalc(game.player.itemaccesslevel - 1, 56000));
    if (game.player.money > PriceCalc(game.player.itemaccesslevel - 1, 56000)) {
        game.player.money -= PriceCalc(game.player.itemaccesslevel - 1, 56000);
        game.player.itemaccesslevel++;
    }
}
function upgrade(item) {
    console.log(PriceCalc(item.posgainLevel, item.posgainBasePrice));
    if (game.player.money > PriceCalc(item.posgainLevel, item.posgainBasePrice)) {
        game.player.money -= PriceCalc(item.posgainLevel, item.posgainBasePrice);
        item.posgainLevel++;
        item.posgain++;
    }
}
function upgradeActiv(item) {
    console.log(PriceCalc(item.activgainLevel, item.activgainBasePrice));
    if (game.player.money > PriceCalc(item.activgainLevel, item.activgainBasePrice)) {
        game.player.money -= PriceCalc(item.activgainLevel, item.activgainBasePrice);
        item.activgainLevel++;
        item.activgain++;
    }
}
function clearTemporaryInventory() {
    game.player.temporaryInventoryActive = false;
    for (let i = 0; i < game.player.temporaryInventory.length; i++) {
        const element = game.player.temporaryInventory[i];

        element.amount = 0;
    }
}

/*
<div>Du kannst diese Items finden:</div>
    <li v-for="item in items">
        {{ item.posgain }}x {{ item.name }}
    </li>
    
 */
var app = new Vue({
    el: '#app',
    data: {
        contents: game.player.inventory,
        player: game.player,
        inventoryOpen: false,
        menu: false,
        time: 5,
        interval: 0,
        items: items
    },
    methods: {
        registerNewPlayer: () => { },
        closeInventory: () => { menu = false; },
        searchForItems: function () {
            this.inventoryOpen = false;

            //Click Delay auf dem Hauptklickerbutton
            if (this.time >= 0) {
                for (let i = 0; i < game.player.inventory.length; i++) {
                    const element = game.player.inventory[i];

                    let gained = ~~(Math.random() * element.posgain);
                    element.amount += gained;
                    game.player.temporaryInventoryActive = true;
                    game.player.temporaryInventory[i].amount += gained;
                    game.player.temporaryInventory[i].amountonce = gained;
                }
                clearInterval(this.interval);
                this.interval = setInterval(() => {
                    this.time += 0.5
                    if (this.time == 5.5) {
                        this.time = 5.0;
                        clearInterval(this.interval);

                        clearTemporaryInventory();
                        game.player.temporaryInventoryActive = false;
                    }

                }, 500)
                this.time = 0;
            }
        }
    }
})

